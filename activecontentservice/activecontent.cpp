/*
 *   Copyright 2011 by Aaron Seigo <aseigo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "activecontent.h"

#include <QDBusMetaType>

namespace ActiveContent
{

class ActiveContent::Private
{
public:
    KUrl url;
    QString title;
    QString mimetype;
    QString serviceIdentifier;
    WId windowId;
};

ActiveContent::ActiveContent()
    : d(new Private)
{
    qDBusRegisterMetaType<ActiveContent>();
}

ActiveContent::ActiveContent(KUrl &url,
                             const QString &title,
                             const QString &mimetype,
                             const QString &serviceIdentifier,
                             WId windowId)
    : d(new Private)
{
    d->url = url;
    d->title = title;
    d->mimetype = mimetype;
    d->serviceIdentifier = serviceIdentifier;
    d->windowId = windowId;
};

ActiveContent::ActiveContent(const ActiveContent &other)
    : d(new Private(*other.d))
{
}

ActiveContent::~ActiveContent()
{
    delete d;
}

const ActiveContent &ActiveContent::operator=(const ActiveContent &rhs)
{
    if (this != &rhs) {
        *d = *rhs.d;
    }

    return *this;
}

KUrl ActiveContent::url() const
{
    return d->url;
}

void ActiveContent::setUrl(const KUrl &url)
{
    d->url = url;
}

QString ActiveContent::title() const
{
    return d->title;
}

void ActiveContent::setTitle(const QString &title)
{
    d->title = title;
}

QString ActiveContent::mimetype() const
{
    return d->mimetype;
}

void ActiveContent::setMimeType(const QString &mimetype)
{
    d->mimetype = mimetype;
}

QString ActiveContent::serviceIdentifier() const
{
    return d->serviceIdentifier;
}

void ActiveContent::setServiceIdentifier(const QString &serviceIdentifier) const
{
    d->serviceIdentifier = serviceIdentifier;
}

WId ActiveContent::windowId() const
{
    return d->windowId;
}

void ActiveContent::setWindowId(const WId &windowId)
{
    d->windowId = windowId;
}

} // namespace ActiveContent

QDBusArgument &operator<<(QDBusArgument &argument, const ActiveContent::ActiveContent &content)
{
    argument.beginStructure();
    argument << content.url().url()
             << content.title()
             << content.mimetype()
             << content.serviceIdentifier()
             << (int)content.windowId(); //FIXME: non-portable, relies on X11's definition of WId!
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ActiveContent::ActiveContent &content)
{
    argument.beginStructure();
    QString string;
    argument >> string;
    content.setUrl(string);
    argument >> string;
    content.setTitle(string);
    argument >> string;
    content.setMimeType(string);
    argument >> string;
    content.setServiceIdentifier(string);
    int winId; //FIXME: non-portable, relies on X11's definition of WId!
    argument >> winId;
    content.setWindowId(winId);
    argument.endStructure();
    return argument;
}

